const items = {
    onehaxet2: {
        wood: 1,
        axehed: 1,
        pegs: 2,
        leatherbands: 6,
        nails: 2
    },
    onehbaxet2: {
        wood: 1,
        axehed: 1,
        pegs: 2,
        leatherbands: 6,
        nails: 2
    }
}
const availibleitems = {
    wepsmithbencht1: ["onehaxet2", "onehbaxet2"]
}
function calc_resources() {
    const bench = document.querySelector("#bench");
    const keys = availibleitems[bench.value];
    const result = [];
    const amount = parseInt(document.querySelector("#amount").value);
    for (const key of keys) {
        const item = { ...items[key] };
        for (const itemkey in item) {
            item[itemkey] = item[itemkey] * amount;
        }
        result.push(item);
    }
    console.log(result);
    return result;
}
function render_item(item, label) {
    return label + " " + item.wood;
}
const dinger = document.querySelectorAll("#bench option");
for (const ding of dinger) {

}